FROM armhf/alpine

RUN apk -q update \
    && apk -q --no-progress add mpd \
    && apk -q --no-progress add mpc \
    && rm -rf /var/cache/apk/*

RUN mkdir -p /var/lib/mpd/music \
    && mkdir -p /var/lib/mpd/playlists \
    && mkdir -p /var/lib/mpd/database \
    && mkdir -p /var/log/mpd/mpd.log \
    && chown -R mpd:mpd /var/lib/mpd \
    && chown -R mpd:mpd /var/log/mpd/mpd.log

RUN addgroup mpd && adduser -D mpd -G audio
VOLUME ["/var/lib/mpd/music", "/var/lib/mpd/playlists", "/var/lib/mpd/database"]
COPY mpd.conf /etc/mpd.conf

EXPOSE 6600

CMD ["mpd", "--stdout", "--no-daemon"]
