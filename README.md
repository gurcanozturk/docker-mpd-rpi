# docker-mpd-rpi

Music Play Daemon in docker for Raspberry Pi (armhf)

# Build it.
docker build -t docker-mpd-rpi .

# Run it.
docker run --name mpd -d -p 6680:6680 docker-mpd-rpi

# Connect it.
Use any MPD client to connect <docker_ip>:6680
